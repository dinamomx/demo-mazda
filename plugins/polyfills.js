/**
 * Este archivo es para aglomerar y configrurar los polyfills necesarios para cada proyecto
 * Está desactivado por defecto
 * Tendrás que instalar manualmente los paquetes
 */

/* eslint-disable import/no-unresolved */
/* eslint-disable import/no-extraneous-dependencies */


// Es para dar soporte al css object-fit a IE
const objectFitImages = require('object-fit-images')

// Es para dar soporte a imagenes responsivas en IE/Edge 15 o menor
const picturefill = require('picturefill')
require('picturefill/dist/plugins/mutation/pf.mutation.min.js')

window.onNuxtReady(() => {
  objectFitImages()
  picturefill()
})
